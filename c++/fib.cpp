#include<iostream>

unsigned long fib(unsigned long n) {
  unsigned long a = 0;
  unsigned long b = 1;
  for (unsigned long i = 1; i < n; ++i) {
    unsigned long temp = a;
    a = b;
    b += temp;
  }
  return b;
}


int main() {
  std::cout << fib(75) << std::endl;
}