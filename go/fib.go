package main

func main() {
	println(fib(75))
}

func fib(n uint64) uint64 {
	var a uint64 = 0;
	var b uint64 = 1;
	for i := n; i > 1; i-- {
		temp := a;
		a = b;
		b += temp;
	}
	return b
}
