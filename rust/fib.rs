
fn main() {
    println!("{}", fib(75));
}

fn fib(n: u64) -> u64{
    let mut a: u64 = 0;
    let mut b: u64 = 1;
    for _ in 1..n {
        let temp = a;
        a = b;
        b += temp;
    }
    b
}
